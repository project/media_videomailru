<?php
/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle Video.mail.ru videos.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetVideoMailRuHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    if ($id = $this->getID($embedCode)) {
      return file_stream_wrapper_uri_normalize('videomailru://v/' . $id);
    }
  }

  public function getID($embedCode) {
    // http://my.mail.ru/corp/afisha/video/89/31496.html
    $patterns = array(
      '@\.mail\.ru/(.+)video/(.+)\.html@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1]) && isset($matches[2])) {
        return $matches[1] . $matches[2];
      }
    }
    return FALSE;
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    // Try to default the file name to the video's title.
    if (empty($file->fid) && $info = $this->getInfo()) {
      $file->filename = truncate_utf8($info['name'], 255);
      $file->metadata['thumbnail'] = $info['thumbnail'];
    }

    return $file;
  }

  /**
   * Returns information about the media.
   */
  public function getInfo() {
    $id = $this->getID($this->embedCode);
    $parts = explode('/', $id);
    $last = array_pop($parts);
    $last = "p-$last.jpg";
    array_push($parts, $last);
    return array(
      'name' => $id,
      'thumbnail' => 'http://content.video.mail.ru/'. implode('/', $parts),
    );
  }
}
