<?php
/**
 *  @file
 *  Extends the MediaReadOnlyStreamWrapper class to handle Video.mail.ru videos.
 */

/**
 *  Create an instance like this:
 *  $videomailru = new MediaVideoMailRuStreamWrapper('videomailru://v/[video-code]');
 */
class MediaVideoMailRuStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://videoapi.my.mail.ru/videos/embed/';

  public function _videomailru_get_code() {
    list(, $code) = explode('://v/', $this->uri);
    return $code;
  }
  /**
   * Handles parameters on the URL string.
   */
  public function interpolateUrl() {
    return $this->base_url . $this->_videomailru_get_code() . '.html';
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/videomailru';
  }

  function getOriginalThumbnailPath() {
    $file = file_uri_to_object($this->uri);
    return $file->metadata['thumbnail'];
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    // There's no need to hide thumbnails, always use the public system rather
    // than file_default_scheme().
    $local_path = 'public://media-videomailru/' . $this->_videomailru_get_code() . '.jpg';

    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());

      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }

    return $local_path;
  }
}
