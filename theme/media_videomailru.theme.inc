<?php

/**
 * @file
 * Theme and preprocess functions for Media: Video.mail.ru.
 */

/**
 * Preprocess function for theme('media_videomailru_video').
 */
function media_videomailru_preprocess_media_videomailru_video(&$variables) {
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $id = $wrapper->_videomailru_get_code();
  $variables['video_id'] = check_plain($id);

  // Make the file object available.
  $file = file_uri_to_object($variables['uri']);
  $variables['id'] = $file->fid;
  // The embed code from mail.ru usually uses these width and height.
  $x = 626;
  $y = 367;

  if (isset($variables['options']['width'])) {
    $variables['width'] = $variables['options']['width'];
    $variables['height'] = ceil($variables['width'] * $y / $x);
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file->filename);
  $variables['alternative_content'] = $variables['title'];

  // Build the iframe URL.
  $variables['url'] = url($wrapper->getExternalUrl(), array('query' => array('width' => $variables['width'], 'height' => $variables['height']), 'external' => TRUE));
}
