<?php
/**
 * @file
 *
 * Template file for theme('media_videomailru_video').
 *
 * Variables available:
 *  $uri - The media uri for the Video.mail.ru video (e.g., videomailru://v/corp/afisha/89/31496).
 *  $video_id - The unique identifier of the Video.mail.ru video (e.g., 123456).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Video.mail.ru iframe.
 *  $options - An array containing the Media Video.mail.ru formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Video.mail.ru file display options.
 *  $height - The height value set in Media: Video.mail.ru file display options.
 *  $title - The Media: Video.mail.ru file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 */

?>
<div class="<?php print $classes; ?> media-videomailru-<?php print $id; ?>">
  <iframe class="media-videomailru-player" width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="no" scrolling="no" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""><?php print $alternative_content; ?></iframe>
</div>